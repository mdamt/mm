var dataApps = {
  "accessories": {
      title: "Aksesoris"
      , children: [
        {
          name: "gedit"
          , title: "Penyunting Teks"
          , icon: "accessories-text-editor.svg"
          , shot: "gedit.png"
        }
        , {
          name: "gcalctool"
          , title: "Kalculator"
          , icon: "accessories-calculator.svg"
          , shot: "kalkulator.png"
        }
        , {
          name: "stardict"
          , title: "Kamus"
          , icon: "accessories-dictionary.svg"
          , shot: "kamus.png"
        }
      ]
  }
  , "internet" : {
    title: "Internet"
    , children: [
      {
        name: "web"
        , title: "Peramban Web"
        , icon: "chromium.svg"
        , shot: "web.png"
      }
      , {
        name: "twitter"
        , title: "Twitter"
        , icon: "twitter.svg"
        , shot: "twitter.png"
      }
      , {
        name: "ngobrel"
        , title: "Ngobrel"
        , icon: "internet-group-chat.svg"
        , shot: "ngobrel.png"
      }
    ]
  }
  , "office" : {
    title: "Perkantoran"
    , children: [
      {
        name: "writer"
        , title: "Mengetik"
        , icon: "libreoffice-writer.svg"
        , shot: "writer.png"
      }
      , {
        name: "calc"
        , title: "Lembar Sebar"
        , icon: "libreoffice-calc.svg"
        , shot: "calc.png"
      }
    ]
  }
}


