var Manokwari = Manokwari || function() {
  var shotData = {};
  var iconData = {};
  var runningApps = {};
  var windowStack = [];
  var applicationData = {};
  var height = 0;

  var recalculateHeight = function() {
    height = document.height;
    
  }

  var secondaryMenuMoveUp = function(e) {
    recalculateHeight();
    e.css("top", "-" + e.height() + "px");
    e.css("opacity", "0");
  }

  var secondaryMenuMoveDown = function(e) {
    recalculateHeight();
    e.css("top", e.height() + "px");
    e.css("opacity", "0");
  }

  var secondaryMenuDisplay = function(me) {
    var current = 100000;
    var after = false;
    var myIndex = -1;
    $(".menu-container-secondary-content").each(function(i, e) {
      if ($(e).css("top") == "0px") {
        current = i;
      }
      if ($(e).attr("id") == me.attr("id")) {
        myIndex = i;
        if (i > current) {
          after = true;
        }
      } 
    })

    // two conditions:
    // - selected item is after current item
    //   * current item is pushed up
    //   * selected item is coming from bottom
    // - selected item is before current item
    //   * current item is pushed down 
    //   * selected item is coming from up 
    if (after) {
      secondaryMenuMoveUp($($(".menu-container-secondary-content")[current]));
      secondaryMenuMoveDown($($(".menu-container-secondary-content")[myIndex]));
    } else {
      secondaryMenuMoveUp($($(".menu-container-secondary-content")[myIndex]));
      secondaryMenuMoveDown($($(".menu-container-secondary-content")[current]));
    }
    me.css("top", "0px");
    me.css("opacity", "1.0");
  }

  // Setups the desktop
  // It creates desktop and menu-event-box as well as connecting the events
  var setupDesktop = function() {
    $("body")
      .append($("<div>").addClass("desktop"))
      .append($("<div>").addClass("menu-event-box"))
      .append($("<img>").attr("src", "distribution-logo.png").addClass("menu-launcher-button"))
      .append($("<img>").attr("src", "icons/user-desktop.svg").addClass("desktop-launcher-button"))


    // Setups the menu launcher button to mousedown
    $(".menu-launcher-button").bind("mousedown", function() {
      $(".sub-menu-entry").removeClass("active");
      if ($(".menu-container-primary-open").length > 0) {
        closeMenuEntries();
      } else {
        openMenuEntries();
        openSubMenuEntries($(".sub-menu-entry")[0]);
      }
    })

    $(".desktop-launcher-button").bind("mousedown", function() {
      if ($(".menu-container-launcher-open").length > 0) {
        closeDesktopLauncher();
      } else {
        openDesktopLauncher();
      }
    })

    // Setups the menu-event-box area to mousedown
    $(".menu-event-box").bind("mousedown", function() {
      closeMenuEntries();
      closeDesktopLauncher();
    })
  }

  // Setups sub menu entries
  // It creates the chevrons and connecting the events
  var setupSubMenuEntries = function() {
    // Adds right chevron to sub-menu-entry
    $(".sub-menu-entry").each(function() {
      if (!$(this).hasClass("no-chevron")) {
        $(this).append($("<i>").addClass("icon-chevron-right pull-right hide"))  
      }
    })

    $(".sub-menu-entry").unbind();
    $(".sub-menu-entry").bind("mousedown", function(e) {
      e.preventDefault();
      var hasDataId = $(this).attr("data-id");
      if (hasDataId) {
        $(".sub-menu-entry").removeClass("active");
        openSubMenuEntries(this);
      } else {
        if ($(this).attr("data-app")) {
          launchApp($(this));
        }
        closeMenuEntries();
      }
    })

    $(".sub-menu-entry").bind("mouseover", function(e) {
      $(this).children("i").show(); 
    })

    $(".sub-menu-entry").bind("mouseout", function(e) {
      $(this).children("i").hide(); 
    })

    $(".menu-container-secondary-content").css("top", height + "px");
  }

  // Opens desktop launcher 
  var openDesktopLauncher = function() {
    $(".menu-container-launcher").addClass("menu-container-launcher-open").removeClass("menu-container-launcher-closed");
    $(".menu-event-box").addClass("menu-event-box-open").removeClass("menu-event-box-closed");
  }

  // Closes desktop launcher 
  var closeDesktopLauncher = function() {
    $(".menu-container-launcher").addClass("menu-container-launcher-closed").removeClass("menu-container-launcher-open");
    $(".menu-event-box").addClass("menu-event-box-closed").removeClass("menu-event-box-open");
  }

  // Opens menu entries
  var openMenuEntries = function() {
    $(".menu-container-primary").addClass("menu-container-primary-open").removeClass("menu-container-primary-closed");
    $(".menu-container-secondary").addClass("menu-container-secondary-open").removeClass("menu-container-primary-closed");
    $(".menu-event-box").addClass("menu-event-box-open").removeClass("menu-event-box-closed");
  }

  // Closes menu entries
  var closeMenuEntries = function() {
    $(".menu-container-primary").addClass("menu-container-primary-closed").removeClass("menu-container-primary-open");
    $(".menu-container-secondary").addClass("menu-container-secondary-closed").removeClass("menu-container-secondary-open");
    $(".menu-event-box").addClass("menu-event-box-closed").removeClass("menu-event-box-open");
    $(".menu-container-secondary-content").addClass("menu-container-secondary-closed").removeClass("menu-container-secondary-open");
  }


  // Opens sub menu entries
  var openSubMenuEntries = function(e) {
    $(".menu-container-secondary").addClass("menu-container-secondary-open").removeClass("menu-container-secondary-closed");
    $(".menu-container-secondary-content").addClass("menu-container-secondary-open").removeClass("menu-container-secondary-closed");
    var sub = $(e).attr("data-id")
    secondaryMenuDisplay($("#" + sub)); 
    $(e).addClass("active");
  }

  // Closes sub menu entries
  var closeSubMenuEntries = function() {
    $(".menu-container-secondary").addClass("menu-container-secondary-closed").removeClass("menu-container-secondary-open");
    $(".menu-container-secondary-content").addClass("menu-container-secondary-closed").removeClass("menu-container-secondary-open");
  }


  // Create a sub menu entry
  var createSubMenuEntry = function(item) {
    var entry = $("<li>")
                .append(
                  $("<div>")
                    .unbind()
                    .bind("mousemove", function() {
                      $(this).find("<img>").addClass("sub-menu-icon-enlarged")
                    })
                    .bind("mouseout", function() {
                      $(this).find("<img>").removeClass("sub-menu-icon-enlarged")
                    })
                    .addClass("sub-menu-entry")
                    .attr("data-app", item.name)
                    .text(item.title)
                    .prepend(
                      $("<img>")
                        .attr("src", "icons/" + item.icon)
                        .addClass("sub-menu-icon")
                    )
                )
    shotData[item.name] = item.shot;
    iconData[item.name] = item.icon;
    return entry;
  }

  // Rebuilds menu UI structure
  var rebuildMenu = function() {
    for (var itemName in applicationData) {
      var item = applicationData[itemName];
      if ($("#" + itemName).length > 0) {
        
      } else {
        var menu = $("<div>").attr("id", itemName)
                  .addClass("menu-container-secondary-content")
        var list = $("<ul>").addClass("nav nav-list")
        for (var i = 0; i < item.children.length; i ++) {
          createSubMenuEntry(item.children[i]).appendTo(list);
        }
        menu.append(list);
        menu.appendTo($(".menu-container-secondary"));
        $("#application-category-list").append(
            $("<li>").append(
              $("<div>").addClass("sub-menu-entry")
                        .attr("data-id", itemName)
                        .text(item.title)
            )
          )
      }
    }
  }

  // Updates menu content
  var updateMenuContent = function() {
    applicationData = dataApps; 
    rebuildMenu();
    setupSubMenuEntries();
  }


  // raise a window 
  var raiseWindow = function(win) {
    var rectA = {
      X1: win.position().left, 
      Y1: win.position().top, 
      X2: win.position().left + win.width(),
      Y2: win.position().top + win.height()
    }

    for (var i = 0; i < windowStack.length; i ++) {
      var winB = $("#" + windowStack[i]);
      if (win.attr("id") != winB.attr("id")) { 
        var rectB = {
          X1: winB.position().left, 
          Y1: winB.position().top, 
          X2: winB.position().left + winB.width(),
          Y2: winB.position().top + winB.height()
        }

        // overlap
        if (rectA.X1 < rectB.X2 && rectA.X2 > rectB.X1 &&
            rectA.Y1 < rectB.Y2 && rectA.Y2 > rectB.Y1) {

          if (win.css("z-index") <= winB.css("z-index")) {
            // Very simple rearrangement
            var z = winB.css("z-index");
            if (z == win.css("z-index")) {
              z = win.css("z-index") + 1;
            }
            winB.css("z-index", win.css("z-index"));
            win.css("z-index", z);
          }
        }
      }
    }
  }

  // Launches a fake app
  var launchFakeApp = function(e) {

    var win = $("<img>");
    var id = (new Date).getTime();
    windowStack.push("win-" + id);
    win.attr("draggable", false)
      .attr("id", "win-" + id)
      .attr("src", "shots/" + shotData[e.attr("data-app")])
      .addClass("application-window")
      .appendTo($("body"))
      .unbind()
      .bind("mousedown", function(e) {
        e.preventDefault();
        raiseWindow($(this));
      })
      .bind("mousemove", function(e) {
        e.preventDefault();
        if (e.which == 1) {
          // Handles window movement
          if (e.pageY > $(this).position().top &&
              e.pageY < $(this).position().top + 30) {
            $(this).css("left", e.pageX - 10); 
            $(this).css("top", e.pageY - 10); 
          }
        }
      })
    var icon = $("<img>");
    icon.attr("draggable", false)
    	.attr("id", "icon-" + id)
    	.attr("winid", "win-" + id)
	.attr("src", "icons/" + iconData[e.attr("data-app")])
	.addClass("panel-icon")
	.appendTo("#panel")
        .bind("mousedown", function(e) {
          e.preventDefault();
          raiseWindow($("#win-" + id))
        })
	.popover({
          trigger: "hover",
          html: true,
          placement: "bottom",
          content: "<img src=shots/" + shotData[e.attr("data-app")] + ">"
	})
  }

  // Launches an app
  var launchApp = function(e) {
    launchFakeApp(e);
  }

  var setupLauncher = function(e) {
    $(".launcher-entry").click(function(e) {
      launchApp($(this));
      console.log($(this));
      closeDesktopLauncher();
    });
  }

  $(document).ready(function() {
    recalculateHeight();
    setupDesktop();
    updateMenuContent();
    setupLauncher();
  })
}()
